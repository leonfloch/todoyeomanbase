'use strict';

/**
 * @ngdoc function
 * @name mytodoYeomanApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mytodoYeomanApp
 */
angular.module('mytodoYeomanApp')

  .controller('MainCtrl', function ($scope, localStorageService) {
    
    //los elementos que se muestran
    $scope.tipoLista = {value:'A'};
    
    /**
     * Los datos se almacenan en local storage del navegador
     * se utiliza $watch para que actualize los cambios
     */ 
    var todosInStore = localStorageService.get('todos');
    $scope.todos = todosInStore || [];
    $scope.$watch('todos', function () {
      localStorageService.set('todos', $scope.todos);
    }, true);    
    
                    
    /**
     * adiciona nuevo registro al todo 
     */
    $scope.addTodo = function () {
      var todo = {valor:$scope.valor, completo:false};      
      $scope.todos.push(todo);
      $scope.valor = '';
    };
    
    /**
     * Elimina registro del todo
     */ 
    $scope.removeTodo = function (index) {
      $scope.todos.splice(index, 1);
    };
    
    //metodo que define que registros se muestran
    $scope.muestraReg = function(item) {
      if ($scope.tipoLista.value === 'T') {
        return true;
      }
      if ($scope.tipoLista.value === 'A') {
        return !item.completo;
      }
      if ($scope.tipoLista.value === 'C') {
        return item.completo;
      }
    };   
    
    /**
     * Valida que boton debe estar activo
     */ 
    $scope.botonActivo = function(tipoLis) {
      var estilo = 'btn btn-default';
      if (tipoLis === $scope.tipoLista.value) {
        estilo = 'btn btn-primary';
      }      
      return estilo;
    };  
    
  });
