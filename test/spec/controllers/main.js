'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('mytodoYeomanApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('Debe arrancar sin datos', function () {
    expect(scope.todos.length).toBe(0);
  });

  it('Debe adicionar un elemento a la lista', function () {        
    scope.valor = 'Test 1';
    scope.addTodo();
    expect(scope.todos.length).toBe(1);
  });

  it('debe borrar el elemento adicionado', function () {
    scope.valor = 'Test 1';
    scope.addTodo();
    scope.removeTodo(0);
    expect(scope.todos.length).toBe(0);
  });
  
});
