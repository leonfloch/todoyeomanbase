'use strict';

/**
 * @ngdoc function
 * @name mytodoYeomanApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mytodoYeomanApp
 */
angular.module('mytodoYeomanApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
